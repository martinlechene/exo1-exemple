<?php


class Eleve{

  private $nom;
  private $prenom;
  private $age;

  public function __construct($nom){
    $this->nom = $nom;
  }

  public function getNom($majuscules = false){
    $nom = $this->nom;
    if($majuscules){
      $nom = strtoupper($nom);
      //return strtoupper($this->nom);
    }
    return $nom;
  }
  public function setNom($nom){

    $this->nom = $nom;
  }

  public function setPrenom($p){

    $this->prenom = $p;
  }

  /**
    retourne le nom complet sous forme
    de chaine de caractère
    @return string
  **/
  public function getNomComplet(){
      return $this->getNom().' '.$this->prenom;
  }

  /**
    @return Adresse
  */
  public function getAdresse(){
      return $this->adresse;
  }

  public function setAdresse(Adresse $adresse){

    $this->adresse = $adresse;
  }


  /**
    @return Age
   */
  public function getAge(){
    return $this->age->getAge();
  }
  public function setAge(Age $age){
    $this->age = $age;
  }
}

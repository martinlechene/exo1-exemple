<html style="background-color:black">
    <?php

// importation des fichiers de classes
require_once __DIR__.'/Adresse.php';
require_once __DIR__.'/Eleve.php';
require_once __DIR__.'/Age.php';
require_once __DIR__.'/Formatter.php';

// instanciation de l'élève
$eleve = new Eleve('Lechêne');
$eleve->setPrenom('Martin');

// instanciation de l'adresse
$adresse = new Adresse();

$adresse->setRue('Rue D. Duck');
$adresse->setNum(42);
$adresse->setCP(4020);
$adresse->setVille('Liege');


$eleve->setAdresse($adresse);

$age = new Age("25");

$eleve->setAge($age);

// utilisation de la méthode statique `formatEleve` de la classe Formatter

echo Formatter::formatEleve($eleve);
//

?>

</html>
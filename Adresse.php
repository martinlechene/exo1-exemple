<?php

class Adresse {

  private $rue;
  private $num;
  private $cp;
  private $ville;

  public function __construct($rue="", $num=0, $cp=0, $ville="Liege"){
    $this->rue = $rue;
    $this->num = $num;
    $this->cp = $cp;
    $this->ville = $ville;
  }
  public function getRue(){
    return $this->rue;
  }

  public function getNum(){
    return $this->num;
  }

  public function getCP(){
    return $this->CP;
  }

  public function getVille(){
    return $this->ville;
  }

  public function setRue($rue){
    $this->rue = $rue;
  }
  public function setNum($num){
    $this->num = $num;
  }
  public function setCP($cp){
    $this->cp = $cp;
  }
  public function setVille($ville){
    $this->ville = $ville;
  }

  public function getAdresseInfo(){
    return sprintf('%s %d %d %s',
      $this->rue,
      $this->num,
      $this->cp,
      $this->ville
    );
  }
}

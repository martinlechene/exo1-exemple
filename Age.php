<?php

class Age{

  private $age;
 

  public function __construct($age){
    $this->age = $age;
  }

  public function setAge($age){
    $this->age = $age;
  }
  public function getAge(){
    return $this->age;
  }
}

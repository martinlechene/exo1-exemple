<?php

class Formatter{

  private static $format = 'html';

  public static function formatEleve(Eleve $eleve){
    if(self::$format != 'html'){
      return 'html';
    }
    return sprintf(
      ' 
      <style>
      *{
        color:white
      }
      </style>
      Nom Prénom :%s</center>
        <br /><hr />
        Age : <center style="color:white">%s ans</center>
        <br /><hr />
        Localisation : <center style="color:white">%s</center>
      </center>
      ',
      $eleve->getNomComplet(),
      $eleve->getAge(),
      $eleve->getAdresse()->getAdresseInfo()
    );

  }
}
